package Crocopfe.demo.service;

import java.util.List;

import Crocopfe.demo.models.MaisonModel;



public interface MaisonService {

	
	
	public List<MaisonModel> getAllMaison();
	public MaisonModel getMaisonById(Long id);
	public void deleteMaisonById(Long id);
	public MaisonModel addMaison(MaisonModel m);
	public MaisonModel editMaison(MaisonModel m);
}
