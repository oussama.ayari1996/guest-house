import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { SignupServiceService } from 'src/app/Services/signup-service.service';
import { TokenStorageService } from 'src/app/Services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm: FormGroup;
  
  user: any = {};
  isLoginFailed: boolean;
  isLoggedIn: boolean;
  roles: any = [];
  error: string;
  constructor(private fb: FormBuilder, private signup:SignupServiceService,private router: Router,private tokenStorage: TokenStorageService,
   
    ) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['123456789', Validators.required]
    });
  }
  login() {
    
    this.user.email = this.loginForm.get('email').value;
    this.user.password = this.loginForm.get('password').value;

    this.error = ''; // Clear any previous error messages

    this.signup.login(this.user).subscribe(
      (data) => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;

        // Set the user data here
        this.signup.setUser(data); // Assuming user data is available in the response
        // Now the user data is available for the route guard

     this.router.navigate(['/home'])
      },
      (error) => {
        if (error.status === 401) {
          // Handle unauthorized (failed login) response
          this.error = 'Invalid email or password. Please try again.';
        } else {
          // Handle other errors
          this.error = 'An error occurred during login. Please try again later.';
          console.error('An error occurred during login:', error);
        }
      }
    );
  }
}





