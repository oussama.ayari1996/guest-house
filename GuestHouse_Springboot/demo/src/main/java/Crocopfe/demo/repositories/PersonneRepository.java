package Crocopfe.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import Crocopfe.demo.models.Personne;

public interface PersonneRepository extends JpaRepository<Personne, Long> {

}
