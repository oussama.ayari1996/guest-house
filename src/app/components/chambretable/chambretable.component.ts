import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChambreService } from 'src/app/Services/chambre.service';

@Component({
  selector: 'app-chambretable',
  templateUrl: './chambretable.component.html',
  styleUrls: ['./chambretable.component.css']
})
export class ChambretableComponent implements OnInit {
  owners: any=[]
  imageData: string;
  constructor(private router:Router, private mService:ChambreService) { }

  ngOnInit() {
    this.mService.getAllChambre().subscribe((response)=>{this.owners=response;})
    //this.loadImage();
    
      }
 
    
    deleteChambre(id:any){
    
      this.mService.deleteChambreById(id).subscribe(
        
        (response) =>{console.log("here BE",response); this.mService.getAllChambre().subscribe((response)=>{this.owners=response;})});
    
      }
    navigate(id:any){
      this.router.navigate(["addchambre/"+id])
      }



    loadImage() {
      this.mService.getAllChambre().subscribe((Chambres: any) => {
        // Assuming each Chambre object has an 'image' property with Base64 image data
        this.owners = Chambres;
        
        // You can loop through the owners and set the imageData for each owner
        this.owners.forEach((owner: any) => {
          owner.imageData = 'data:image/jpeg;base64,' + owner.image;
        });
      });
    }
    
    }
