import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChambreService {
  constructor(private http: HttpClient) { }

  registrationUrl = "http://localhost:8086/api/chambre";

  addChambre(chambre: any, maisonId: number) {
    return this.http.post<{ data: any }>(`${this.registrationUrl}/${maisonId}`, chambre);
  }

  editChambre(chambre: any) {
    return this.http.put<{ data: any }>(`${this.registrationUrl}/${chambre.id}`, chambre);
  }

  getChambreById(id: any) {
    return this.http.get<{ x: any }>(`${this.registrationUrl}/${id}`);
  }

  deleteChambreById(id: any) {
    return this.http.delete<{ data: any }>(`${this.registrationUrl}/${id}`);
  }

  getAllChambre() {
    return this.http.get<{ Chambre: any }>(this.registrationUrl);
  }
}
