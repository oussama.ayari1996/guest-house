import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/Services/token-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private user: any; 
  setUser(user: any) {
    this.user = user;
  }
  constructor(private tokenStorageService: TokenStorageService) { }

  ngOnInit() {

  }

  isOwnerOrAdmin(): boolean {
    const user = this.tokenStorageService.getUser();
    
    return user && (user.rrole.includes('owner') || user.rrole.includes('admin'));
  }
}

  

