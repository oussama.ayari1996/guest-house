import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SignupServiceService {
  private user: any; 
  setUser(user: any) {
    this.user = user;
  }

  userUrl: string = "http://localhost:8086/api/auth" ;
 constructor (private http: HttpClient) {}
 // user = {firstName, lastName, email , password ....}
 signup(user) {
 return this.http.post( this.userUrl + "/register" , user);
 }
 // user = {email,password}
 login(user) {
 return this.http.post<{ accessToken: any }>( this.userUrl + "/login", user);
 }

 // auth.service.ts

 isAdminOrOwner(): boolean {
  
  console.log("User Object:", this.user);
  if (this.user && this.user.rrole) {
    const userRole = this.user.rrole;
    return userRole === 'admin' || userRole === 'owner';
  }
  return false;
}





}