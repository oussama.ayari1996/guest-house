import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ContactComponent } from './components/contact/contact.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwnerDashboardComponent } from './components/owner-dashboard/owner-dashboard.component';
import { AddMaisonComponent } from './components/add-maison/add-maison.component';
import { AddChambreComponent } from './components/add-chambre/add-chambre.component';
import { InterceptorService } from './Services/interceptor.service';
import { ChambretableComponent } from './components/chambretable/chambretable.component';
import { MaisontableComponent } from './components/maisontable/maisontable.component';
import { ReservationComponent } from './components/reservation/reservation.component';
import { FilterNomPipe } from './pipes/filter-nom.pipe';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { TablestyleDirective } from './directive/tablestyle.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    ContactComponent,
    OwnerDashboardComponent,
    AddMaisonComponent,
    AddChambreComponent,
    ChambretableComponent,
    MaisontableComponent,
    ReservationComponent,
    FilterNomPipe,
    NotFoundComponent,
    TablestyleDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,// TDF
    ReactiveFormsModule, //reactive
    HttpClientModule,
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
