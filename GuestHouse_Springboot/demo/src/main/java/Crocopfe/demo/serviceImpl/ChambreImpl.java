package Crocopfe.demo.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Crocopfe.demo.models.ChambreModel;
import Crocopfe.demo.repositories.ChambreRepository;
import Crocopfe.demo.service.ChambreService;

	@Service

	public class ChambreImpl implements ChambreService {
		@Autowired
		private ChambreRepository ChambreRepo;
		@Override
		public List<ChambreModel> getAllChambre() {
			return ChambreRepo.findAll();
		}

		@Override
		public ChambreModel getChambreById(Long id) {
	Optional<ChambreModel> c = ChambreRepo.findById(id);
			
			return c.isPresent()? c.get():null;
		}

		@Override
		public void deleteChambreById(Long id) {
			ChambreRepo.deleteById(id);
			
		}

		@Override
		public ChambreModel addChambre(ChambreModel c) {
			return ChambreRepo.save(c);
		}

		@Override
		public ChambreModel editChambre(ChambreModel c) {
			return ChambreRepo.save(c);
		}

		
	}

