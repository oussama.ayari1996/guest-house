import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MaisonService {
  

  constructor(private http: HttpClient) { }

  registrationUrl = "http://localhost:8086/api/maison";
  
  addMaison(Maison: any) { 
    return this.http.post<{ data: any }>(this.registrationUrl, Maison);
  }

  editMaison(Maison: any) { 
    return this.http.put<{ data: any }>(this.registrationUrl, Maison);
  }

  getMaisonById(id: any) {
    return this.http.get<{ x: any }>(`${this.registrationUrl}/${id}`);
  }

  deleteMaisonById(id: any) {
    return this.http.delete<{ data: any }>(`${this.registrationUrl}/${id}`);
  }

  getAllMaison() {
    return this.http.get<{ Maison: any }>(this.registrationUrl);
  }

  saveMaison(maisonData: FormData): Observable<any> {
    return this.http.post(`${this.registrationUrl}`, maisonData);
  }
}

