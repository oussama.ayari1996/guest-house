package Crocopfe.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



import Crocopfe.demo.models.ClientModel;
import Crocopfe.demo.repositories.ClientRepository;
import Crocopfe.demo.service.ClientService;

@RestController
@RequestMapping("/api/register")
@CrossOrigin("*")

public class RegistrationController {

    @Autowired
    private ClientService clientService;

    //@PostMapping
    //public ResponseEntity<?> addClient(@RequestBody ClientModel newUser) {
        // Check if the username is already taken
      //  if (userRepository.findByEmail(newUser.getEmail()) != null) {
   //         return ResponseEntity.badRequest().body("Username is already taken.");
     //   }
        // Set the user role (you can set it based on the "user" or "owner" selection)
       // newUser.setRole("user");
        //userRepository.save(newUser);
        //return ResponseEntity.ok("User registered successfully.");
    //}
    @PostMapping
	public ClientModel addClient(@RequestBody ClientModel c) {
	return clientService.addClient(c); }
}

