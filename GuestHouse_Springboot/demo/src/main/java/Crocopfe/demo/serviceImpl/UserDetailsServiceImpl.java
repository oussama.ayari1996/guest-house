package Crocopfe.demo.serviceImpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import Crocopfe.demo.models.User;
import Crocopfe.demo.models.UserDetailsImpl;

import Crocopfe.demo.repositories.UserRepositoryy;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UserRepositoryy userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(email)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with email: " + email));
		return UserDetailsImpl.build(user);
	}

}