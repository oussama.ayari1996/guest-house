package Crocopfe.demo.service;

import java.util.List;

import Crocopfe.demo.models.OwnerModel;

public interface OwnerService {

	
	
	public List<OwnerModel> getAllOwner();
	public OwnerModel getOwnerById(Long id);
	public void deleteOwnerById(Long id);
	public OwnerModel addOwner(OwnerModel o);
	public OwnerModel editOwner(OwnerModel o);
}
