package Crocopfe.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="Personne")
public class Personne {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="PERSONNE_ID")
	private Long id;
	private String nom;
	private String prenom;
	private String address;
	private String phoneNumber;
	private String email;
	public Personne() {
	
	}
	public Personne(Long id, String nom, String prenom, String address, String phoneNumber, String email) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getId() {
		return id;
	}
	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", address=" + address + ", phoneNumber="
				+ phoneNumber + ", email=" + email + "]";
	}
	
	
	
}
