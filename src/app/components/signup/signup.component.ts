import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SignupServiceService } from 'src/app/Services/signup-service.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user: any = {}
  id: any
  signupForm: FormGroup;
  path:any="";

  constructor(private activatedRoute: ActivatedRoute,private fb: FormBuilder,private http: HttpClient,private register:SignupServiceService,private router:Router) {
    this.signupForm = this.fb.group({
      rrole: ['', Validators.required],
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, Validators.pattern('^[0-9]*$'), this.phoneValidator]],
      address: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', Validators.required],
    }, {
      validators:this.MustMatch("password","confirmPassword")
    });

  }
  ngOnInit() {

  }
  signup() {

   
    if (!this.signupForm.invalid) {
    console.log(this.signupForm.value);
    this.register.signup(this.signupForm.value).subscribe((data) => {
    this.router.navigate(["login"]);
    });
    }
   
    }

  phoneValidator(control: FormControl) {
    let phoneNumber = control.value;
    if (phoneNumber>0 && phoneNumber < 8) {
      return { minLength: true };
    }
    return null;
  }
  
  
  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}


    

 
  }

  


