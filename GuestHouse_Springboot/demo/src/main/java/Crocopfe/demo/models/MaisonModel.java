package Crocopfe.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="Maison")
public class MaisonModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="MAISON_ID")
	private Long id;
	private String name;
	private String address;
	private String description;
	private String city;
	
	public MaisonModel() {	}

	public MaisonModel(Long id, String name, String address, String description, String city) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.description = description;
		this.city = city;
	}

	public Long getId() {
		return id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "MaisonModel [id=" + id + ", name=" + name + ", address=" + address + ", description=" + description
				+ ", city=" + city + "]";
	}
	
	
	
	
	
	
}
