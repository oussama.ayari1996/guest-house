import { Component, OnInit,EventEmitter, Input, Output } from '@angular/core';
import { ChambreService } from 'src/app/Services/chambre.service';
import { MaisonService } from 'src/app/Services/maison.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @Input() maison : any ; chambre:any
  @Output()  newItemEvent = new EventEmitter<any>()
  constructor(private mService:MaisonService,private cService:ChambreService) { }
  owners: any=[]
  chambres: any=[]
  
  ngOnInit() {
    this.mService.getAllMaison().subscribe((response)=>{this.owners=response;})
    this.cService.getAllChambre().subscribe((response)=>{this.chambres=response;})
    this.loadImage();
  }



  passId(id:any){
    console.log("here id maison into child com",this.maison.id);
    this.newItemEvent.emit(this.maison.id)
    
    
      }
      


      loadImage() {
        this.mService.getAllMaison().subscribe((maisons: any) => {
         
          this.owners = maisons;
          
          
          this.owners.forEach((owner: any) => {
            owner.imageData = 'data:image/jpeg;base64,' + owner.image;
          });
        });
      }
}
