package Crocopfe.demo.service;

import java.util.List;

import Crocopfe.demo.models.ClientModel;

public interface ClientService {

	
	public List<ClientModel> getAllClient();
	public ClientModel getClientById(Long id);
	public void deleteClientById(Long id);
	public ClientModel addClient(ClientModel c);
	public ClientModel editClient(ClientModel c);
}
