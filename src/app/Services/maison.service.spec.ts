import { TestBed } from '@angular/core/testing';

import { MaisonService } from './maison.service';

describe('MaisonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaisonService = TestBed.get(MaisonService);
    expect(service).toBeTruthy();
  });
});
