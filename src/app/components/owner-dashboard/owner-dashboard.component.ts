import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MaisonService } from 'src/app/Services/maison.service';

@Component({
  selector: 'app-owner-dashboard',
  templateUrl: './owner-dashboard.component.html',
  styleUrls: ['./owner-dashboard.component.css']
})
export class OwnerDashboardComponent implements OnInit {
  owners: any=[]
  imageData: string;
  constructor(private router:Router, private mService:MaisonService) { }

  ngOnInit() {
    this.mService.getAllMaison().subscribe((response)=>{this.owners=response;})
    this.loadImage();
    
      }
 
    
    deleteMaison(id:any){
    
      this.mService.deleteMaisonById(id).subscribe(
        (response) => {
          console.log("Delete response", response);
          // Call loadImage() here
          this.loadImage();
        },
        (error) => {
          console.error("Error deleting maison", error);
        }
      );
    }
    
    navigate(id:any){
    this.router.navigate(["/ownerdashboard"])
    }



    loadImage() {
      this.mService.getAllMaison().subscribe((maisons: any) => {
        // Assuming each maison object has an 'image' property with Base64 image data
        this.owners = maisons;
        
        // You can loop through the owners and set the imageData for each owner
        this.owners.forEach((owner: any) => {
          owner.imageData = 'data:image/jpeg;base64,' + owner.image;
        });
      });
    }
    
    }

  

