package Crocopfe.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Crocopfe.demo.models.MaisonModel;
import Crocopfe.demo.service.MaisonService;



@RequestMapping("api/maison")
@CrossOrigin("*")
@RestController
public class MaisonController {
	@Autowired
	private MaisonService maisonService;
	
	@GetMapping
	public List<MaisonModel> getAllMaison(){
	return maisonService.getAllMaison();
	}
	
	@GetMapping("/{id}")
	public MaisonModel getMaison(@PathVariable Long id) {
		
		return maisonService.getMaisonById(id);
	}
	@DeleteMapping("/{x}")
	public void deleteMaisonById(@PathVariable Long x) {
		maisonService.deleteMaisonById(x);
	}
	
	@PostMapping
	public MaisonModel addMaison(@RequestBody MaisonModel m) {
	return maisonService.addMaison(m); }

	@PutMapping
	public MaisonModel editMaison(@RequestBody MaisonModel m) {
		return maisonService.editMaison(m);
	}
	
}


