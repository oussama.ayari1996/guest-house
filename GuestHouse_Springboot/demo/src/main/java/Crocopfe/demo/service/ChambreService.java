package Crocopfe.demo.service;

import java.util.List;

import Crocopfe.demo.models.ChambreModel;

public interface ChambreService {

	
		public List<ChambreModel> getAllChambre();
		public ChambreModel getChambreById(Long id);
		public void deleteChambreById(Long id);
		public ChambreModel addChambre(ChambreModel c);
		public ChambreModel editChambre(ChambreModel c);
	}

