package Crocopfe.demo.service;

import java.util.List;

import Crocopfe.demo.models.UserModel;

public interface UserService {
	public List<UserModel> getAllUser();
	public UserModel getUserById(Long id);
	public void deleteUserById(Long id);
	public UserModel addUser(UserModel u);
	public UserModel editUser(UserModel u);
}
