import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SignupServiceService } from './Services/signup-service.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: SignupServiceService, private router: Router) {}

  canActivate(): boolean {
    console.log("AuthGuard canActivate called");
    if (this.authService.isAdminOrOwner()) {
      console.log("User has admin or owner role. Allowing access.");
      return true;
    } else {
      console.log("User does not have admin or owner role. Redirecting to notfound.");
    
      this.router.navigate(['/notfound']);
      return false;
    }
  }
  
}
