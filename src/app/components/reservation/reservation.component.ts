import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; // Import ActivatedRoute
import { ChambreService } from 'src/app/Services/chambre.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  chambre: any = {};
  id: any; 

  constructor(private cService: ChambreService, private route: ActivatedRoute) { }

  ngOnInit() {
   
    this.id=this.route.snapshot.paramMap.get('id')
    this.cService.getChambreById(this.id).subscribe((response)=> {this.chambre=response})
  }
  }

