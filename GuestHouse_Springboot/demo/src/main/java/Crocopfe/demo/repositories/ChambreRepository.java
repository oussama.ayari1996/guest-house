package Crocopfe.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Crocopfe.demo.models.ChambreModel;

@Repository
public interface ChambreRepository extends JpaRepository<ChambreModel, Long> {

}
