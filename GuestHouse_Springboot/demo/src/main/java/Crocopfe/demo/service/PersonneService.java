package Crocopfe.demo.service;

import java.util.List;

import Crocopfe.demo.models.Personne;


public interface PersonneService {
	public List<Personne> getAllPersonne();
	public Personne getPersonneById(Long id);
	public void deletePersonneById(Long id);
	public Personne addPersonne(Personne p);
	public Personne editPersonne(Personne p);
}
