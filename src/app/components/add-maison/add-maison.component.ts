import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MaisonService } from 'src/app/Services/maison.service';

@Component({
  selector: 'app-add-maison',
  templateUrl: './add-maison.component.html',
  styleUrls: ['./add-maison.component.css']
})
export class AddMaisonComponent implements OnInit {

  addMaisonForm!: FormGroup
  Maison: any = { }
  id: any
  title: string = "add Maison"
  imageName:any
  owner:any=[];
  //user = sessionStorage.getItem("USER_KEY")
  ownerId:number;
  selectedFile: File | null = null;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private mService:MaisonService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id")
    if (this.id) {
      this.title = "update Maison";
      this.getMaisonById()
    }
  }
  editMaison() {
    if (this.id) {
     this.mService.editMaison(this.Maison).subscribe( (response)=>{console.log("here BE",response); this.mService.getAllMaison().subscribe((response)=>{this.Maison=response;})});
    
    }
   // else {
     //this.mService.addMaison(this.Maison).subscribe();

    //}
    this.router.navigate(["/maisontable"])
    
  }
  getMaisonById() {
    this.mService.getMaisonById(this.id).subscribe( (response)=> {console.log("here Maison by id",response);
    this.Maison=response});
    

      }


      onFileChange(event: any) {
        const fileInput = event.target;
        if (fileInput.files && fileInput.files.length > 0) {
          this.Maison.image = fileInput.files[0];
        }
      }

      onSubmit() {
        if (this.Maison.name && this.Maison.image) {
          const formData = new FormData();
          formData.append('name', this.Maison.name);
          formData.append('address', this.Maison.address);
          formData.append('description', this.Maison.description);
          formData.append('city', this.Maison.city);
          formData.append('image', this.Maison.image);
          
        
      
          if (this.id) {
            // Update an existing Maison
            formData.append('id', this.id); // Include the Maison's ID for updating
            this.mService.saveMaison(formData).subscribe( (response)=>{console.log("here BE",response); this.mService.getAllMaison().subscribe((response)=>{this.Maison=response;})});
                this.Maison = {};
                this.router.navigate(["/maisontable"]);
              
          } else {
            // Add a new Maison
            this.mService.saveMaison(formData).subscribe(
              (response) => {
                console.log('Maison added successfully');
                // Optionally, you can reset the form fields after a successful submission:
                this.Maison = {};
                this.router.navigate(["/maisontable"]);
              },
              (error) => {
                console.error('Error occurred while adding Maison:', error);
              }
            );
          }
        }
        this.router.navigate(["/maisontable"])
      }
      

    }
    
    