package Crocopfe.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Crocopfe.demo.models.OwnerModel;

@Repository
public interface OwnerRepository extends JpaRepository<OwnerModel, Long> {

}
