import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { OwnerDashboardComponent } from './components/owner-dashboard/owner-dashboard.component';
import { AddMaisonComponent } from './components/add-maison/add-maison.component';
import { AddChambreComponent } from './components/add-chambre/add-chambre.component';
import { ChambretableComponent } from './components/chambretable/chambretable.component';
import { MaisontableComponent } from './components/maisontable/maisontable.component';
import { ReservationComponent } from './components/reservation/reservation.component';
import { AuthGuard } from './auth.guard';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [  
  {path: '', component: HomeComponent} ,
  {path: 'home', component: HomeComponent} ,
  {path:"login",component:LoginComponent},
  {path:"signup",component:SignupComponent},
  {path:"signup-owner",component:SignupComponent},
  {path:"signup-client",component:SignupComponent},
  {path:"ownerdashboard",component:OwnerDashboardComponent,  canActivate: [AuthGuard]},
 // {path:"addmaison",component:AddMaisonComponent},
  {path:"addmaison/:id",component:AddMaisonComponent},
  {path:"chambretable",component:ChambretableComponent},
  {path:"chambretable/:id",component:ChambretableComponent},
 // {path:"maisontable",component:MaisontableComponent},
  {path:"maisontable/:id",component:MaisontableComponent},
  {path:"reservation",component:ReservationComponent},
  {path:"addchambre",component:AddChambreComponent},
  {path:"addchambre/:id",component:AddChambreComponent},
  {path:"notfound",component:NotFoundComponent},
  {
    path: 'addmaison',
    component: AddMaisonComponent,
    canActivate: [AuthGuard] 
  },
  {
    path: 'maisontable',
    component: MaisontableComponent,
    canActivate: [AuthGuard] 
  },
  {path:"reservation",component:ReservationComponent},
  {path:"reservation/:id",component:ReservationComponent},
  
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
