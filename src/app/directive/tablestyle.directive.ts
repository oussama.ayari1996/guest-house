import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appTablestyle]'
})
export class TablestyleDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  @HostListener('mouseenter') onMouseEnter() {
    this.changeBackgroundColor('aqua'); // Change the background color on mouse enter
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.changeBackgroundColor(''); // Reset the background color on mouse leave
  }

  private changeBackgroundColor(color: string) {
    this.renderer.setStyle(this.el.nativeElement, 'background-color', color);
  }
}
