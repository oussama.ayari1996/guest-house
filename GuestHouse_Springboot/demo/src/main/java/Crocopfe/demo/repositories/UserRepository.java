package Crocopfe.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;


import Crocopfe.demo.models.UserModel;

public interface UserRepository  extends JpaRepository<UserModel, Long> {

}
