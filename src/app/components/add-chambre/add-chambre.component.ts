import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ChambreService } from 'src/app/Services/chambre.service';
import { MaisonService } from 'src/app/Services/maison.service';

@Component({
  selector: 'app-add-chambre',
  templateUrl: './add-chambre.component.html',
  styleUrls: ['./add-chambre.component.css']
})
export class AddChambreComponent implements OnInit {

 
  addChambreForm!: FormGroup
  Chambre: any = {};
  maison:any=[];
  id: any
  maisonId:number;
  title: string = "add Chambre"
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private mService:ChambreService,private maisonService:MaisonService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    if (this.id) {
      this.title = "update Chambre";
      this.getChambreById();
    }
  
    this.maisonService.getAllMaison().subscribe((response) => {
      this.maison = response; 
      
      this.maisonId=this.maison[0].id;
      
    });
  }
  
  addChambre() {
    this.Chambre.maisonId=this.maisonId;
    if (this.id) {

     this.mService.editChambre(this.Chambre).subscribe((response)=> {console.log("here BE",response); this.mService.getAllChambre().subscribe((response)=>{this.Chambre=response;})})
    }
    else {
     this.mService.addChambre(this.Chambre,this.maisonId).subscribe((response)=> {console.log("here BE",response); this.mService.getAllChambre().subscribe((response)=>{this.Chambre=response;})})
     this.router.navigate(["/chambretable"])
     

    }
  
    
  }
  getChambreById() {
    this.mService.getChambreById(this.id).subscribe( (response)=> {console.log("here Chambre by id",response);
    this.Chambre=response});
    
      }

      handleFileInput(event: any): void {
        const file = event.target.files[0];
        this.Chambre.image = file;
    }
    

    selectMaison(evt){
console.log("here event ID",evt.target.value);
this.maisonId=evt.target.value;   }
    }


