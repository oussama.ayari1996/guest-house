import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChambretableComponent } from './chambretable.component';

describe('ChambretableComponent', () => {
  let component: ChambretableComponent;
  let fixture: ComponentFixture<ChambretableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChambretableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChambretableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
