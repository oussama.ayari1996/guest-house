package Crocopfe.demo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Crocopfe.demo.models.ClientModel;
import Crocopfe.demo.repositories.ClientRepository;

import Crocopfe.demo.service.ClientService;

@Service
public class ClientImpl implements ClientService {
	@Autowired
	private ClientRepository clientRepo;
	@Override
	public List<ClientModel> getAllClient() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ClientModel getClientById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteClientById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ClientModel addClient(ClientModel c) {
		return clientRepo.save(c);
	}

	@Override
	public ClientModel editClient(ClientModel c) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
