import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaisontableComponent } from './maisontable.component';

describe('MaisontableComponent', () => {
  let component: MaisontableComponent;
  let fixture: ComponentFixture<MaisontableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaisontableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaisontableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
