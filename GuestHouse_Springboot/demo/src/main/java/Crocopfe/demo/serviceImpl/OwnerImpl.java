package Crocopfe.demo.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import Crocopfe.demo.models.OwnerModel;
import Crocopfe.demo.repositories.OwnerRepository;
import Crocopfe.demo.service.OwnerService;

@Service
public class OwnerImpl implements OwnerService {
	@Autowired
	private OwnerRepository ownerRepo;

	@Override
	public List<OwnerModel> getAllOwner() {
		return ownerRepo.findAll();
	}

	@Override
	public OwnerModel getOwnerById(Long id) {
Optional<OwnerModel> o = ownerRepo.findById(id);
		
		return o.isPresent()? o.get():null;
	}

	@Override
	public void deleteOwnerById(Long id) {
		ownerRepo.deleteById(id);
		
	}

	@Override
	public OwnerModel addOwner(OwnerModel o) {
		return ownerRepo.save(o);
	}

	@Override
	public OwnerModel editOwner(OwnerModel o) {
		return ownerRepo.save(o);
	}
	
}
