package Crocopfe.demo.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Crocopfe.demo.models.MaisonModel;
import Crocopfe.demo.repositories.MaisonRepository;
import Crocopfe.demo.service.MaisonService;

@Service

public class MaisonImpl implements MaisonService {
	@Autowired
	private MaisonRepository MaisonRepo;
	@Override
	public List<MaisonModel> getAllMaison() {
		return MaisonRepo.findAll();
	}

	@Override
	public MaisonModel getMaisonById(Long id) {
Optional<MaisonModel> o = MaisonRepo.findById(id);
		
		return o.isPresent()? o.get():null;
	}

	@Override
	public void deleteMaisonById(Long id) {
		MaisonRepo.deleteById(id);
		
	}

	@Override
	public MaisonModel addMaison(MaisonModel o) {
		return MaisonRepo.save(o);
	}

	@Override
	public MaisonModel editMaison(MaisonModel o) {
		return MaisonRepo.save(o);
	}

	
}

