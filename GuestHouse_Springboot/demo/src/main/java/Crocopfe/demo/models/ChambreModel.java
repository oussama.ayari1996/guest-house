package Crocopfe.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

@Entity
@Table (name="Chambre")
public class ChambreModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="CHAMBRE_ID")
	
	private Long id;
	private String name;
	private String capacity;
	private String description;
	private String price;
	

	  
	  
	public ChambreModel() {
	
	}

	public ChambreModel(Long id, String name, String capacity, String description, String price) {
		super();
		this.id = id;
		this.name = name;
		this.capacity = capacity;
		this.description = description;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "ChambreModel [id=" + id + ", name=" + name + ", capacity=" + capacity + ", description=" + description
				+ ", price=" + price + "]";
	}
	
	
	
	
	
	
}
