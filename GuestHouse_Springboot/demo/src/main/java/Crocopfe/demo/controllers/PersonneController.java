package Crocopfe.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Crocopfe.demo.models.Personne;
import Crocopfe.demo.service.PersonneService;

@RequestMapping("api/personne")
@CrossOrigin("*")
@RestController
public class PersonneController {
	@Autowired
	private PersonneService personneService;
	
	@GetMapping
	public List<Personne> getAllPersonne(){
	return personneService.getAllPersonne();
	}
	
	@GetMapping("/{id}")
	public Personne getPersonne(@PathVariable Long id) {
		
		return personneService.getPersonneById(id);
	}
	@DeleteMapping("/{x}")
	public void deletePersonneById(@PathVariable Long x) {
		personneService.deletePersonneById(x);
	}
	
	@PostMapping
	public Personne addPersonne(@RequestBody Personne m) {
	return personneService.addPersonne(m); }

	@PutMapping
	public Personne editPersonne(@RequestBody Personne m) {
		return personneService.editPersonne(m);
	}
	
}


