package Crocopfe.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Crocopfe.demo.models.ClientModel;

@Repository
public interface ClientRepository extends JpaRepository<ClientModel, Long> {
	ClientModel findByEmail(String email);
}
