package Crocopfe.demo.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import Crocopfe.demo.models.UserModel;
import Crocopfe.demo.repositories.UserRepository;

import Crocopfe.demo.service.UserService;

@Service
public class UserImpl implements UserService {
	@Autowired
	private UserRepository userRepo;
	@Override
	public List<UserModel> getAllUser() {
		return userRepo.findAll();
	}

	@Override
	public UserModel getUserById(Long id) {
Optional<UserModel> o = userRepo.findById(id);
		
		return o.isPresent()? o.get():null;
	}

	@Override
	public void deleteUserById(Long id) {
		userRepo.deleteById(id);
		
	}

	@Override
	public UserModel addUser(UserModel o) {
		return userRepo.save(o);
	}

	@Override
	public UserModel editUser(UserModel o) {
		return userRepo.save(o);
	}

	
}
