package Crocopfe.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Crocopfe.demo.models.ChambreModel;
import Crocopfe.demo.service.ChambreService;

@RequestMapping("api/chambre")
@CrossOrigin("*")
@RestController
public class ChambreController {
	@Autowired
	private ChambreService chambreService;
	
	@GetMapping
	public List<ChambreModel> getAllChambre(){
	return chambreService.getAllChambre();
	}
	
	@GetMapping("/{id}")
	public ChambreModel getChambre(@PathVariable Long id) {
		
		return chambreService.getChambreById(id);
	}
	@DeleteMapping("/{x}")
	public void deleteChambreById(@PathVariable Long x) {
		chambreService.deleteChambreById(x);
	}
	
	@PostMapping
	public ChambreModel addChambre(@RequestBody ChambreModel c) {
	return chambreService.addChambre(c); }

	@PutMapping
	public ChambreModel editChambre(@RequestBody ChambreModel c) {
		return chambreService.editChambre(c);
	}
	
}

